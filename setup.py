from setuptools import setup

setup(
    name='neo',
    version='0.2.0',
    py_modules=['neo'],
    install_requires=[
        'Click',
    ],
    entry_points='''
        [console_scripts]
        neo=neo:cli
    ''',
)

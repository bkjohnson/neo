import requests
import click
from datetime import date, timedelta, datetime
import yaml
from pathlib import Path

def get_settings():
    defaults = {
            'api_key': 'DEMO_KEY'
        }
    settings = {}
    settings_path = str(Path.home()) + '/.config/neo/config.yml'
    try:
        file_config = open(settings_path, 'r')
        settings = yaml.full_load(file_config )
    finally:
        return {**defaults, **settings }

def str_to_date(ctx, param, value):
    return datetime.strptime(value, '%Y-%m-%d').date()

@click.command()
@click.option('--date', default=date.today, callback=str_to_date, help='day to search for asteroids (YYYY-MM-DD)')
@click.option('--days', default=1, help='Number of days to search following the start date')
def cli(date, days):
    settings = get_settings()

    payload = { "start_date": date, "end_date": date + timedelta(days - 1), "api_key": settings['api_key'] }
    res = requests.get("https://api.nasa.gov/neo/rest/v1/feed", params = payload)
    data = res.json()

    params = { 'count': data['element_count'] }
    output = "There were {count} asteroids "
    if days > 1:
        output += "from {start} to {end}"
        params['start'] = date
        params['end'] = date + timedelta(days - 1)
    else:
        output += "on {date}"
        params['date'] = date

    print(output.format(**params))
